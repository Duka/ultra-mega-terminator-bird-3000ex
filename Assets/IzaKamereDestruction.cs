﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IzaKamereDestruction : MonoBehaviour
{
	[SerializeField] private Transform ovajObject;

    void Update()
    {
        if(ovajObject.position.x <= -15 || ovajObject.position.y <= -7) Destroy(gameObject);
    }
}
