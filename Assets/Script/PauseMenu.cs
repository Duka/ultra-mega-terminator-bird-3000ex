﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class PauseMenu : MonoBehaviour
{
    public static bool PausedGame = false;
    public GameObject pauseMenuUI;
    // Update is called once per frame
    void Awake()
    {
    	PausedGame = false;
    	pauseMenuUI.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
        	if(PausedGame) Resume();
        	else Pause();
        }
    }

    public void Resume()
    {
    	pauseMenuUI.SetActive(false);
    	Time.timeScale = 1f;
    	PausedGame = false;
    }

    public void Pause()
    {
    	pauseMenuUI.SetActive(true);
    	Time.timeScale = 0f;
    	PausedGame = true;
    }

    public void GoBack()
    {
    	PausedGame = false;
		Debug.Log("Loading level");
		SceneManager.LoadScene("Title");
    }
}
