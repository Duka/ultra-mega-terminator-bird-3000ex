﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGenerator : MonoBehaviour
{
    [SerializeField] private GameObject buildingPrefab;
	[SerializeField] private Transform thisBuilding;
	[SerializeField] private Transform birbPlayer;

	[SerializeField] private GameObject MinePrefab;
	[SerializeField] private GameObject BoxPrefab;
	[SerializeField] private GameObject BoxTprefab;
	[SerializeField] private GameObject BoxLprefab;
	[SerializeField] private GameObject BoxTowerPrefab;
	

	private int RandomNum;
	private int BrojKutija;
	private int BrojMina;

	void Start(){
		RandomNum = Random.Range((int)1,(int)4);
		if(RandomNum == 2 || RandomNum == 4) Kutije();
		if(RandomNum == 3 || RandomNum == 4) Mine();
	}

    void FixedUpdate()
    {	
        if(birbPlayer != null && birbPlayer.position.x  - thisBuilding.position.x >= 2.5) Destroy(gameObject);        
    }

    void Kutije(){
    	BrojKutija = Random.Range((int)1, (int)4);
    	
    		if(BrojKutija == 1) Instantiate(BoxTprefab, new Vector3(Random.Range(thisBuilding.position.x -2, thisBuilding.position.x + 2), thisBuilding.position.y + 5.5f, thisBuilding.position.z), thisBuilding.rotation);
    		else if(BrojKutija == 2) Instantiate(BoxLprefab, new Vector3(Random.Range(thisBuilding.position.x -3, thisBuilding.position.x), thisBuilding.position.y + 5.5f, thisBuilding.position.z), thisBuilding.rotation);
    		else if(BrojKutija == 3) Instantiate(BoxTowerPrefab, new Vector3(Random.Range(thisBuilding.position.x, thisBuilding.position.x), thisBuilding.position.y + 5.5f, thisBuilding.position.z), thisBuilding.rotation);
    		else if(BrojKutija == 4) Instantiate(BoxPrefab, new Vector3(Random.Range(thisBuilding.position.x -3, thisBuilding.position.x), thisBuilding.position.y + 5.5f, thisBuilding.position.z), thisBuilding.rotation);

    }
    //birbPlayer

    void Mine(){
    	BrojMina = Random.Range((int)1, (int)2);
    	
    	for(int i = 0; i < BrojMina; i++){
    		//Instantiate(MinePrefab, new Vector3(Random.Range(thisBuilding.position.x -2, thisBuilding.position.x + 2), Random.Range(4,7), 0), thisBuilding.rotation);
            Instantiate(MinePrefab, new Vector3(Random.Range(thisBuilding.position.x -2, thisBuilding.position.x + 2), Random.Range(4,7), 0), birbPlayer.rotation);
    	}    	
    }
}
