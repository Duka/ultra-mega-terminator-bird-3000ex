﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 



public class TitleButtons : MonoBehaviour
{
	//public bool ExitTitle = false;
	public GameObject BackgroundImage;
	public Animator BackgroundAnimations;

	private void Start()
	{
		BackgroundAnimations = BackgroundImage.GetComponent<Animator>();
	}

	void Update()
	{
		if(Input.GetKeyDown("s")) playButton();
	}
	public void playButton()
	{
		BackgroundAnimations.SetTrigger("ExitTheDamnScene");
		StartCoroutine(ExitTheDamnGame());
		Debug.Log("Loading level");
	}

    public void quitButton()
    {  
        Debug.Log("Quit game");  
        Application.Quit();
    }
    
    IEnumerator ExitTheDamnGame(){
    	yield return new WaitForSeconds(1.8f);
    	SceneManager.LoadScene("Lvel");
    }
}
