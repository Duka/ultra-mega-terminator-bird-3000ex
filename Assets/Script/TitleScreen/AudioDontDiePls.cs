﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioDontDiePls : MonoBehaviour
{

	private static AudioDontDiePls instance = null;
	public static AudioDontDiePls Instance; 
    void Awake()
    {
    	if(instance != null && instance != this)
    	{
    		Destroy(this.gameObject);
    	}
    	else
    	{
    		instance = this;
    	}

    	DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
