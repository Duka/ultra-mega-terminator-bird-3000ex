﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLoop : MonoBehaviour {
	
	[SerializeField] private Transform ovajObjekat;
    
    void FixedUpdate()
    {
        if(ovajObjekat.position.x <= -16f) ovajObjekat.position += new Vector3(16f + 34.6f - 0.3f, 0f, 0f);
    }
}
