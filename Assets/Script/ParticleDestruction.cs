﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestruction : MonoBehaviour
{
	public float timeLeft = 10;

	void FixedUpdate()
    {
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0.0f) 
			{
				Destroy(this.gameObject);
			}
    }
}
