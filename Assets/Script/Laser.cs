﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
	[SerializeField] private Transform thisLaser;

    public float speed = 20f;
    public Rigidbody2D rb;

    void Start()
    {
    	rb.velocity = transform.right * speed;    
    }
    void Update()
    {
    	if(thisLaser.position.x >= 16) Destroy(this.gameObject);
    }

    void OnCollisionEnter2D(Collision2D collisionCheck)
    {
  		Destroy(gameObject); 
   	}

}