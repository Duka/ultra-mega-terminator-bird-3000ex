﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirbObjectCreatin : MonoBehaviour
{

	[SerializeField] private GameObject buildingPrefab;
	[SerializeField] private GameObject CoinPrefab;
	[SerializeField] private Transform player;
	[SerializeField] private float Granice = 1f;

	private float StariRandom = 0;
	private float SadaRandom;
	private int i = 0;
	private int CoinOrBuilding = 0;

	void Start(){
		StartCoroutine(SpawnObject());
	}
    
    void FixedUpdate()
    {
    	    
    }

	IEnumerator SpawnObject(){
		while(true){
			Debug.Log("A");
			CoinOrBuilding = Random.Range((int)1, (int)5);
			i = 0;
			do{
				SadaRandom = Random.Range(18,25);
				i++;
				if(i == 5){
					Debug.Log("Sefe, stani");
					break;
				} 
			}while(SadaRandom > StariRandom - Granice && SadaRandom < StariRandom + Granice);
			
			//if(CoinOrBuilding != 3) Instantiate(buildingPrefab, new Vector3(Random.Range(18,25) , Random.Range(-2.5f, -6.5f), Random.Range(-0.40f,0.40f)), player.rotation);
			if(CoinOrBuilding != 3) Instantiate(buildingPrefab, new Vector3(Random.Range(18,25) , Random.Range(-2.5f, -6.5f), Random.Range(-0.025f,0.025f)), player.rotation);
			else Instantiate(CoinPrefab, new Vector3(Random.Range(18,25), Random.Range(-2, 4), 0), player.rotation);
			StariRandom = SadaRandom;
			yield return new WaitForSeconds(1.2f);
		}
	}
}
