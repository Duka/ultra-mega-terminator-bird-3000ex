﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    
    public Transform ovo;
    [SerializeField] private Transform birbPlayer;

    public GameObject particlePrefab;

    void Update(){
		if(birbPlayer.position.x  - ovo.position.x >= 2.5|| ovo.position.y <= -7) Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D collisionCheck)
    {
  		if(collisionCheck.gameObject.tag == "Laser")
  		{
  			Instantiate(particlePrefab, ovo.position, ovo.rotation);
  			Destroy(gameObject);
  		}  
   	}
}
