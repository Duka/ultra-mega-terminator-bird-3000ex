using System.Collections;
using System.Collections.Generic;
using UnityEngine;
  

public class BirbMovment : MonoBehaviour
{

  [SerializeField] private Rigidbody2D rb;
  [SerializeField] private DeathButtons dB;

  public int GndCheck = 1;
  public int doubleJumpCheck = 1;

  public TryAgainButton buttonTime;

  [SerializeField] private GameObject deathText;
  [SerializeField] private GameObject deathParticles;
  [SerializeField] private Transform Player;
  [SerializeField] private float jmpForce = 50;
  [SerializeField] private float iAmSpeed = 1;

  [SerializeField] private AudioSource coinSound;
    void Start()
    {
      PauseMenu.PausedGame = false;
      
      GameObject _deathTextObject = GameObject.FindGameObjectWithTag("deathTextObject");

      buttonTime = _deathTextObject.GetComponent<TryAgainButton>();
    }

    void Update()
    {

      //Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.8f), Vector2.down * 1.3f);
      //Debug.DrawRay(transform.position, Vector2.down * 2f);
      //RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.down* 3f);
      //RaycastHit2D hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y - 0.8f), Vector2.down, 1.1f);
      /*RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 2f);
      if(hit)
      {
        if(hit.collider.tag != "Birb") { Debug.Log("Hit: " + hit.collider.tag); }
        if(hit.collider.tag == "aaa") 
        {
          GndCheck = 1;
          doubleJumpCheck = 1;
          Debug.Log("Castano je!!");
        }
      }*/
        
      if (Input.GetKey("d")) transform.Translate(Vector2.right * iAmSpeed* Time.deltaTime);
      if (Input.GetKey("a")) transform.Translate(Vector2.left * iAmSpeed* Time.deltaTime);
      
      if(Input.GetKeyDown("w") && (GndCheck == 1 || doubleJumpCheck == 1)){
        
        rb.velocity = Vector2.up * jmpForce;
        
        if(GndCheck != 0) GndCheck = 0;
        else doubleJumpCheck = 0;
        //GndCheck = 0;
      } 

      
      if(Player.position.y <= -5.5){
        death();
      }
    }

  void OnCollisionEnter2D(Collision2D collisionCheck) //exit = false
    {

      if(collisionCheck.gameObject.tag == "aaa" || collisionCheck.gameObject.tag == "Coin")
      {
        GndCheck = 1;
        doubleJumpCheck = 1;
        Debug.Log("gnd = 1");
      }

      if(collisionCheck.gameObject.tag == "Mine")
      {
        death();
      }
      
    } 

  void OnTriggerEnter2D(Collider2D col)
    {
      if(col.gameObject.tag == "Coin")
      {
          GndCheck = 1;
          doubleJumpCheck = 1;
          Debug.Log(GndCheck +" " + doubleJumpCheck);
          coinSound.Play();
          Destroy(col.gameObject);
      }
    }

      void death(){
        dB.buttonsOnDeath();
        Instantiate(deathParticles, Player.position, Player.rotation);
        //Instantiate(deathText, new Vector3(20, 2, 0), Player.rotation);
        if(buttonTime.isDead != null) buttonTime.isDead = true;
        Destroy(this.gameObject);
        //Application.LoadLevel(Application.loadedLevel);
      }
}
