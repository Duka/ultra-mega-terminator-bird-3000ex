﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSprite : MonoBehaviour
{

	//[SerializeField] private Sprite[3] sprArr;
	[SerializeField] private Sprite Sprite1;
	[SerializeField] private Sprite Sprite2;
	[SerializeField] private Sprite Sprite3;
	private float i;

    // Start is called before the first frame update
    void Start()
    {
    	
		i = Random.Range(0f,6f);
		if(i <= 2) this.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite1;
		else if(i <= 4) this.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite2;
		else this.gameObject.GetComponent<SpriteRenderer>().sprite = Sprite3;
		
		//this.gameObject.GetComponent<SpriteRenderer>().sprite = sprArr[Random.Range((int)0,(int)2)]; 
    }

}
