using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathButtons : MonoBehaviour
{
    public GameObject highScoreCount;
    public GameObject tryAgainButton;

    void Start()
    {
        neVidim();
        //highScoreCount.SetActive(false);
        //tryAgainButton.SetActive(false);
    }

    public void buttonsOnDeath()
    {
        vidimGa();
    	//highScoreCount.SetActive(true);
    	//tryAgainButton.SetActive(true);
    }

    void neVidim()
    {
        GameObject.Find ("YouDiedButton").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find ("HighscorePanel").transform.localScale = new Vector3(0, 0, 0);

    }
    void vidimGa()
    {
        GameObject.Find ("YouDiedButton").transform.localScale = new Vector3(2.4974f, 2.4974f, 2.4974f);
        GameObject.Find ("HighscorePanel").transform.localScale = new Vector3(1, 1, 1);

    }

}
