﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //public PauseMenu shootCheck;

    public Transform firePoint;
    public GameObject bulletPrefab;

    [SerializeField] private AudioSource fireSound;

    void Update()
    {
    	if(Input.GetButtonDown("Fire1") && PauseMenu.PausedGame == false){
            fireSound.Play();
    		shoot();
    	}   
    }

    void shoot(){
    	Debug.Log("Boom");
    	Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}
