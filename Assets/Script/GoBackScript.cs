﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GoBackScript : MonoBehaviour
{
	public GameObject BackgroundImage;
	public Animator BackgroundAnimations;

	private void Start()
	{
		BackgroundAnimations = BackgroundImage.GetComponent<Animator>();
	}

    public void backTotTitle()
	{
		BackgroundAnimations.SetTrigger("ExitTheDamnScene");
		Time.timeScale = 1f;
		Debug.Log("Loading level");
		
		StartCoroutine(GoBackToTheTitle());
	}

	IEnumerator GoBackToTheTitle(){
    	yield return new WaitForSeconds(1.4f);
    	SceneManager.LoadScene("Title");
    }
}
