﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoggoScript : MonoBehaviour
{
	[SerializeField] private Transform dogger;

    public TryAgainButton isTheBirdDead;

    private bool Let1 = false;
    private bool Let2 = false;
    private bool goreDole = true;

    void Start()
    {
        GameObject iMeanIsItDead = GameObject.FindGameObjectWithTag("deathTextObject");

        isTheBirdDead = iMeanIsItDead.GetComponent<TryAgainButton>();
    }


    void FixedUpdate()
    {
        if(isTheBirdDead.isDead == true && Let1 == false && Let2 == false)
        {
        	transform.Translate((Vector2.right + Vector2.up) * 0.1f);

        	if(dogger.position.y >= -1f)
        		{ 
        			Let1 = true;
        		}
        }
        else if(Let1 == true)
        {
        	if(goreDole == true)
        	{
        		transform.Translate(Vector2.up * 0.01f);
        		if(dogger.position.y >= -0.94) goreDole = false;
        	}
        	else
        	{
        		transform.Translate(Vector2.down * 0.01f);
        		if(dogger.position.y <= -1.6) goreDole = true;
        	}
        }
        
        if(isTheBirdDead.RestartGame == true && Let1 == true)
        {
        	Let1 = false;
       		Let2 = true;
        }

        if(Let2 == true)
        {
        	transform.Translate((1.5f*Vector2.right + Vector2.up) * 0.2f);
        	if(dogger.position.y >= 7) Application.LoadLevel(Application.loadedLevel);
        }
    }

}
