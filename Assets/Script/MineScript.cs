﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineScript : MonoBehaviour
{
	[SerializeField] private GameObject ParticleTime;
	[SerializeField] private Transform MineTime;
	[SerializeField] private Transform birbPlayer;

	void Update(){
		if(birbPlayer.position.x  - MineTime.position.x >= 2.5 || MineTime.position.y <= -7) Destroy(gameObject);
	}
   	void OnCollisionEnter2D(Collision2D collisionCheck)	//exit = false
    {
    	if(collisionCheck.gameObject.tag == "Birb")
    	{
    		Instantiate(ParticleTime, MineTime.position, MineTime.rotation);
    		Destroy(gameObject);
    	}
    }
}
