﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PozadinaMovement : MonoBehaviour {
	
	[SerializeField] private float pozadinaMovementSpeed = 6f;
    
    void FixedUpdate()
    {
        transform.Translate(Vector2.left * pozadinaMovementSpeed* Time.deltaTime);
    }
}
