﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButton : MonoBehaviour
{
	[SerializeField] private GameObject pauseMenuUIResume;

    public void Resume()
    {
    	pauseMenuUIResume.SetActive(false);
    	Time.timeScale = 1f;
    	PauseMenu.PausedGame = false;
    }

}
